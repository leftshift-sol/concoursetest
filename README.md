# Quickstart

1. Setup port-forwarding for 
```
k get pods -n mlpipeline | grep concourse-web
k port-forward concourse-web-SUFFIXHERE -n mlpipeline 8080:8080
```

2. Setup your Concourse CLI team

```
fly --target local login --team-name main --concourse-url http://127.0.0.1:8080
```

Then visit the link in the output of the command, default login is
`test`, default password is `test`

3. Setup pipeline

```
fly -t local sp -p example -c pipeline.yaml -n
```

4. Trigger job

```
fly -t local trigger-job -j example/publish -w
```
